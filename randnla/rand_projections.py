__author__ = 'msingh'

import numpy as np
class BaseProjection(object):
    def __init__(self, matrix):
        self.matrix = matrix
        self.m, self.n = matrix.shape

    def project(self):
        pass


class SRHT(BaseProjection):
    def __init__(self, matrix):
        super(SRHT, self).__init__(matrix)

    def project(self):
        d = np.random.random_integers(0, 1, self.n)
        D = np.diag(d)
