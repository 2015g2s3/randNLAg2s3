__author__ = 'msingh'
import numpy as np

from .rand_sampling import LeverageScoreSampling


def error(y, y_pred):
    return np.linalg.norm(y - y_pred)


class RandLR(LeverageScoreSampling):

    def __init__(self, X, y):
        self.X = X
        self.y = y
        self.m, self.n = X.shape

    def solve(self, r):
        leverage_scores = LeverageScoreSampling(self.X).sampling_probs()
        indices = np.array([i for i in range(self.m)])
        row_indices = np.random.choice(indices, r, p=leverage_scores)
        sampled_lev = leverage_scores[row_indices]
        self.sample_X = self.X[row_indices] / np.sqrt(r * sampled_lev[:, None])
        self.sample_y = self.y[row_indices] / np.sqrt(r * sampled_lev)
        U_, s_, V_ = np.linalg.svd(self.sample_X, full_matrices=False)
        S_ = np.diag(s_)
        # V inv(S) U' y
        # Note the numpy default spits out Vt in svd instead of V.
        w_opt = V_.transpose().dot(np.linalg.inv(S_)).dot(U_.transpose()).dot(
            self.sample_y)
        return w_opt
