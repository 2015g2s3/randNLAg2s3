__author__ = 'msingh'

import numpy as np


class BaseSampling(object):

    def __init__(self, matrix, c=None):
        self.matrix = matrix
        self.c = c

    def sampling_probs(self):
        pass


class NaiveSampling(BaseSampling):

    def __init__(self, matrix, c):
        super(NaiveSampling, self).__init__(matrix, c)

    def sampling_probs(self, rows=True):
        axis = 0 if rows else 1
        norms = np.linalg.norm(self.matrix, axis=axis)
        return norms


class RandomSSampling(BaseSampling):
    """ A random -1, 1 matrix
    """

    def __init__(self, matrix, c):
        super(RandomSSampling, self).__init__(matrix, c)

    def sampling_probs(self, rows=None):
        # ToDo: See if we can directly generate -1,1 matrix
        S = np.random.random_integers(0, 1, size=(self.matrix.shape[1], self.c))
        x_zer, y_zer = np.where(S == 0)
        S[x_zer, y_zer] = -1.0
        return S / np.sqrt(self.c)


class LeverageScoreSampling(BaseSampling):
    """ Leverage Scores based sampling strategy (Exact leverage scores)
    """

    def __init__(self, matrix, k=None):
        super(LeverageScoreSampling, self).__init__(matrix)
        self.U, self.s, self.V = np.linalg.svd(self.matrix, full_matrices=False)
        self.k = k if k else self.s.shape[0]

    def sampling_probs(self):
        if self.matrix_is_tall_skinny:
            return self.row_lev_scores
        return self.column_lev_scores

    @property
    def matrix_is_tall_skinny(self):
        m, n = self.matrix.shape
        return True if m >= n else False

    @property
    def column_lev_scores(self):
        return np.square(np.linalg.norm(self.V, ord=2, axis=0)) / self.k

    @property
    def row_lev_scores(self):
        return np.square(np.linalg.norm(self.U, ord=2, axis=1)) / self.k


class ApproxLevScoreSampling(BaseSampling):

    def __init__(self, matrix):
        super(ApproxLevScoreSampling, self).__init__(matrix)

    def sampling_probs(self):
        pass
